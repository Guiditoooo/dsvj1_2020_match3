#include <iostream>
#include "scenemaster.h"

namespace match {

	namespace config {

		namespace scenes {

			void sceneManager(Scene& scene, Scene nextScene) {

				if (scene != nextScene) {

					switch (scene) //deinit
					{
					case Scene::MENU:
						menu::deinit();
						std::cout << "MENU Deinited";
						break;
					case Scene::GAME:
						game::deinit();
						std::cout << "GAME Deinited";
						break;
					case Scene::CREDITS:
						credits::deinit();
						std::cout << "Credits Deinited";
						break;
					case Scene::RESOLUTION:
						resolution::deinit();
						std::cout << "RESOLUTION Deinited";
						break;
					}


					switch (nextScene)//init
					{
					case Scene::MENU:
						menu::init();
						std::cout << "MENU Inited";
						break;
					case Scene::GAME:
						game::init();
						std::cout << "GAME Inited";
						break;
					case Scene::CREDITS:
						credits::init();
						std::cout << "Credits Inited";
						break;
					case Scene::RESOLUTION:
						resolution::init();
						std::cout << "RESOLUTION Inited";
						break;
					}

					scene = nextScene;

				}

			}

		}

		void generalInit() {

			InitWindow(screen::width, screen::height, "Random Match Game");
			SetTargetFPS(screen::fpsRate);
			InitAudioDevice();

		}

		void generalDeinit() {
			CloseAudioDevice();
		}

	}


	void runGame() {

		config::generalInit();

		do {

			using namespace config;
			using namespace scenes;

			scenes::sceneManager(scene, next_scene);

			switch (scene) {

			case Scene::MENU:
				menu::update();
				menu::draw();
				break;
			case Scene::GAME:
				game::update();
				game::draw();
				break;
			case Scene::CREDITS:
				credits::update();
				credits::draw();
				break;
				case Scene::RESOLUTION:
				resolution::update();
				resolution::draw();
				break;
			case Scene::QUIT:
				menu::continueInGame = false;
				break;

			}

		} while (!WindowShouldClose() && menu::continueInGame);

		config::generalDeinit();

	}



}