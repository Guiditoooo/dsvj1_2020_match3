#ifndef EXTERN_H
#define EXTERN_H

#include "raylib.h"
#include <iostream>

namespace match {

	const float plusSelected = 1.15f;

	enum class Resolution { WIN, LOSE, PLAYING};
	extern Resolution winLoseCheck;

	enum class menuOptions { PLAY = 0, OPTIONS, CREDITS, EXIT, MENU, RESUME, RESOLUTION, NONE };

	struct Texture {

		Texture2D TX;
		Vector2 pos = { 0,0 };
		Color color = WHITE;
		Rectangle hitbox;

	};

	struct Text {

		std::string tx;
		Vector2 pos;
		int size;
		Color color = WHITE;

	};

	struct Button {

		Color color = BLACK;
		Rectangle square;
		menuOptions id;
		bool selected = false;
		Text txt;

	};

	void DrawTX(Texture TX);
	void DebugLines(Texture TX, Color color);
	void LoadMusics();
	void UnloadMusics();
	void drawButton(Button btn);

	namespace config {

		const int mat_width = 10;
		const int mat_height = 8;
		const int min_chain = 4;
		const int squareThickness = 4;

		namespace screen {

			extern int width;
			extern int height;
			extern int fpsRate;

		}

		namespace scenes {

			enum class Scene {
				MENU,
				GAME,
				CREDITS,
				QUIT,
				RESOLUTION,
				NONE
			};

			extern Scene scene;
			extern Scene next_scene;

		}

		Color getRandomColor(int min, int max);

	}

}

#endif