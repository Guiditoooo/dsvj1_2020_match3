#include "game.h"
#include <iostream>

namespace match {

	namespace game {

		float CREATION_TIMER_FIRST = 0.045f;
		float CREATION_TIMER_FAST = 0.01f;

		float frames_expend = 0;

		int sum_y = 0;
		int sum_x = 0;

		int selectedCount = 0;

		bool pausing = false;
		bool bricked = false;

		//aux vars
		bool toDoPause;
		bool toDoObjetives;
		//

		JEWEL selectedJewel = JEWEL::NONE;
		
		Vector2 prevSlotClicked;
		Vector2 lastSlotClicked;
		Vector2 selectedJewelPos[config::mat_height * config::mat_width];

		void checkOneTimeClick(const Vector2& mousePos);
		void setValidToSelect(int x, int y);
		void cleanValidToSet(int x, int y);
		void resetSelectedJewelPos();
		void PauseMenuCheck();

		void init() {

			sum_y = 0;
			sum_x = 0;

			winLoseCheck = Resolution::PLAYING;

			toDoPause = true;
			toDoObjetives = true;

			JewelTX[static_cast<int>(JEWEL::RUBY)] = LoadTexture("res/assets/jewel/ruby.png");
			JewelTX[static_cast<int>(JEWEL::EMERALD)] = LoadTexture("res/assets/jewel/emerald.png");
			JewelTX[static_cast<int>(JEWEL::SAPPHIRE)] = LoadTexture("res/assets/jewel/sapphire.png");
			JewelTX[static_cast<int>(JEWEL::TOPAZ)] = LoadTexture("res/assets/jewel/topaz.png");

			jewelTouched = LoadSound("res/assets/sound/jewel touched.ogg");
			comprobatedChain = LoadSound("res/assets/sound/comprobated chain.ogg");

			for (short i = 0; i < HowManyJewells; i++) {

				JewelTX[i].height = (game_area.height / config::mat_height) * 9 / 10;
				JewelTX[i].width = JewelTX->height;

			}

			prevSlotClicked = { -2,-2 };
			lastSlotClicked = { -2,-2 };

			resetSelectedJewelPos();

			if (toDoPause) {

				using namespace pause;

				#pragma region PAUSE AREA

				pRec.height = config::screen::height / 2;
				pRec.width = config::screen::width / 2;
				pRec.x = config::screen::width / 2 - pRec.width / 2;
				pRec.y = config::screen::height / 2 - pRec.height / 2;

				pText.tx = "PAUSED";
				pText.size = config::screen::height / 11;
				pText.pos.x = config::screen::width / 2 - MeasureText(&pText.tx[0], pText.size) / 2;
				pText.pos.y = pRec.y + pText.size * 5 / 7;
				pText.color = BLACK;

#pragma region RESUME

				resume.txt.tx = "RESUME";
				resume.id = menuOptions::RESUME;
				resume.txt.size = config::screen::height * 2 / 39;
				resume.square.width = MeasureText(&resume.txt.tx[0], resume.txt.size) * 3 / 2;
				resume.square.height = resume.txt.size * 2;
				resume.square.x = pRec.x + pRec.width / 2 - resume.square.width / 2;
				resume.square.y = pText.pos.y + pText.size * 3 / 2;
				resume.color = pauseButtonsColor;
				resume.txt.pos.x = resume.square.x + resume.square.width / 2 - MeasureText(&resume.txt.tx[0], resume.txt.size) / 2;
				resume.txt.pos.y = resume.square.y + resume.square.height / 2 - resume.txt.size / 2;
				resume.txt.color = BLACK;

#pragma endregion

#pragma region MAIN MENU

				mainMenu.txt.tx = "MAIN MENU";
				mainMenu.id = menuOptions::MENU;
				mainMenu.txt.size = config::screen::height * 2 / 39;
				mainMenu.square.width = MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) * 3 / 2;
				mainMenu.square.height = mainMenu.txt.size * 2;
				mainMenu.square.x = pRec.x + pRec.width / 2 - mainMenu.square.width / 2;
				mainMenu.square.y = resume.square.y + pText.size * 3 / 2;
				mainMenu.color = pauseButtonsColor;
				mainMenu.txt.pos.x = mainMenu.square.x + mainMenu.square.width / 2 - MeasureText(&mainMenu.txt.tx[0], mainMenu.txt.size) / 2;
				mainMenu.txt.pos.y = mainMenu.square.y + mainMenu.square.height / 2 - mainMenu.txt.size / 2;
				mainMenu.txt.color = BLACK;

				pauseButtons[0] = resume;
				pauseButtons[1] = mainMenu;

#pragma	endregion

#pragma endregion

			}

			if (toDoObjetives) {

				using namespace objetive;

				#pragma region OBJETIVE

				turnCounter = MIN_TURNS;

				int jewelObjetive = GetRandomValue(MIN_QTY_TOTAL, MAX_QTY_TOTAL);

				turnCounter += jewelObjetive / 11;
				
				for (short i = 0; i < HowManyJewells; i++)
				{
					gemsRequiered[i] = GetRandomValue(0, MAX_QTY_EACH);
					if (jewelObjetive - gemsRequiered[i] >= 0) {
						jewelObjetive -= gemsRequiered[i];
					}
					else
					{
						gemsRequiered[i] = 0;
					}
				}

				for (short i = 0; i < HowManyJewells; i++) {

					objetive::objetiveGems[i].color = WHITE;
					objetive::objetiveGems[i].TX = JewelTX[i];
					objetive::objetiveGems[i].hitbox.width = game::game_area.width / 10;
					objetive::objetiveGems[i].hitbox.height = objetive::objetiveGems[i].hitbox.width;
					objetive::objetiveGems[i].hitbox.x = config::screen::width / 2 - game::game_area.width / 2 + i * (objetive::objetiveGems->hitbox.width) + 
						i * objetive::objetiveGems[i].hitbox.width / (HowManyJewells-1) ;
					objetive::objetiveGems[i].hitbox.y = config::screen::height *4/ 28;
					objetive::objetiveGems[i].TX.width = objetive::objetiveGems[i].hitbox.width * 5 / 7;
					objetive::objetiveGems[i].TX.height = objetive::objetiveGems[i].TX.width;
					objetive::objetiveGems[i].pos.x = objetive::objetiveGems[i].hitbox.x + objetive::objetiveGems[i].hitbox.width / 2 - objetive::objetiveGems[i].TX.width / 2;
					objetive::objetiveGems[i].pos.y = objetive::objetiveGems[i].hitbox.y + objetive::objetiveGems[i].hitbox.height / 2 - objetive::objetiveGems[i].TX.height / 2;

					objetiveText[i].pos.x = objetive::objetiveGems[i].pos.x;
					objetiveText[i].pos.y = config::screen::height * 8 / 28;
					objetiveText[i].size = objetive::objetiveGems[i].hitbox.height * 3 / 4;
					objetiveText[i].color = BLACK;
					objetiveText[i].tx = std::to_string(gemsRequiered[i]);

				}

				req.tx = "Requirements:";
				req.pos.x = objetiveText[0].pos.x;
				req.pos.y = config::screen::height * 2 / 28;
				req.size = objetive::objetiveGems[0].hitbox.height /2;
				req.color = BLACK;

				turnDisplay.tx = "TURNS";
				turnDisplay.size = objetive::objetiveGems[0].hitbox.height / 2;
				turnDisplay.pos.x = config::screen::width - (config::screen::width - game_area.width) / 2 - MeasureText(&turnDisplay.tx[0], turnDisplay.size) * 2;
				turnDisplay.pos.y = config::screen::height * 4 / 28;
				turnDisplay.color = BLACK;

				turn.tx = std::to_string(turnCounter);
				turn.size = objetiveText->size;
				turn.pos.x = turnDisplay.pos.x + MeasureText(&turnDisplay.tx[0], turnDisplay.size) / 2 - MeasureText(&turn.tx[0], turn.size) / 2;
				turn.pos.y = objetiveGems->pos.y + (objetiveGems->TX.height) / 2;
				turn.color = BLACK;




				#pragma endregion

			}

			

		}
		
		void update() {

			if (IsKeyPressed(KEY_T)) {

				config::scenes::next_scene = config::scenes::Scene::RESOLUTION;
				winLoseCheck = Resolution::WIN;

			}

			frames_expend += GetFrameTime();

			if (IsKeyPressed(KEY_P)) {

				pausing = !pausing;
				if (pausing)
					std::cout << "Me pausaste nwn\n";
				else
					std::cout << "Me despausaste uwu\n";

			}
		
			if (!pausing && objetive::turnCounter > 0) {

				setValidToSelect(lastSlotClicked.x, lastSlotClicked.y);

				rollSlots(CREATION_TIMER_FIRST, frames_expend, slot, sum_x, sum_y);

				if (IsKeyPressed(KEY_O)) { // RESETEA GRILLA
					for (short x = 0; x < config::mat_width; x++)
					{
						for (short y = 0; y < config::mat_height; y++)
						{
							slot[x][y].active = false;
						}
					}
					resetGrid();
					rollSlots(CREATION_TIMER_FAST, frames_expend, slot, sum_x, sum_y);
				}

				Vector2 mousePos = GetMousePosition();

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON) && selectedJewel == JEWEL::NONE) {

					checkOneTimeClick(mousePos);

				}

				else {
					
					if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {

						checkHoldClick(mousePos);

					}

				}

				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

					checkReleaseClick();
					std::cout << "\n\n";
				
				}

				for (short x = 0; x < config::mat_width; x++) {

					for (short y = 0; y < config::mat_height; y++) {
						
						if (slot[x][y].canBeDestroyed) {
							createGem(slot[x][y], x, y);
						}

					}

				}
				int checkCount = 0;
				for (short i = 0; i < HowManyJewells; i++) {

					if (objetive::gemsRequiered[i] == 0) {
						checkCount++;
					}

				}
				if (checkCount == 4) {
					winLoseCheck = Resolution::WIN;
				}

			}
			else if (objetive::turnCounter <= 0) {
			
				winLoseCheck = Resolution::LOSE;

			}
			else {

				PauseMenuCheck();

			}

			if (winLoseCheck == Resolution::LOSE || winLoseCheck == Resolution::WIN) {
				config::scenes::next_scene = config::scenes::Scene::RESOLUTION;
			}

		}

		void PauseMenuCheck()
		{
			Vector2 mousePoint = GetMousePosition();

			for (int i = 0; i < pause::howManyPauseButtons; i++) {

				if (CheckCollisionPointRec(mousePoint, pause::pauseButtons[i].square)) {

					pause::pauseButtons[i].selected = true;

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						if (i == 0) {
							config::scenes::next_scene = config::scenes::Scene::GAME;
						}
						else {
							config::scenes::next_scene = config::scenes::Scene::MENU;
							resetGrid();
						}
						pausing = false;
					}

				}
				else {

					pause::pauseButtons[i].selected = false;

				}

			}
		}

		void resetGrid() {

			clearSlots(slot);
			already_created = false;
			init();
			sum_x = 0;
			sum_y = 0;

		}

		void checkReleaseClick() {

			for (short x = 0; x < config::mat_width; x++)
			{
				for (short y = 0; y < config::mat_height; y++)
				{
					if (slot[x][y].selected) {
						selectedCount++;
					}
					if (slot[x][y].validToSelect) {
						slot[x][y].validToSelect = false;
					}
				}
			}

			if (selectedCount >= config::min_chain) {

				for ( short x = 0; x < config::mat_width; x++) {

					for (short y = 0; y < config::mat_height; y++) {

						if (slot[x][y].selected) {

							slot[x][y].canBeDestroyed = true;
							slot[x][y].selected = false;

						}

					}

				}

				std::cout << "Cantidad de entidades seleccionadas: " << selectedCount << "\n";

				objetive::gemsRequiered[static_cast<int>(selectedJewel)] -= selectedCount;

				if (objetive::gemsRequiered[static_cast<int>(selectedJewel)] < 0) {

					objetive::gemsRequiered[static_cast<int>(selectedJewel)] = 0;

				}

				PlaySound(comprobatedChain); // SE ROMPEN
				std::cout << "Deberia estar haciendo ruidito\n";
				objetive::turnCounter--;
				std::cout << "Quedan: " << objetive::turnCounter << " turnos\n";

			}

			else {

				for (short x = 0; x < config::mat_width; x++) {

					for (short y = 0; y < config::mat_height; y++) {

						slot[x][y].selected = false;
						
					}

				}

			}
			lastSlotClicked.x = -2;
			lastSlotClicked.y = -2;
			selectedCount = 0;
			selectedJewel = JEWEL::NONE;

			for (short x = 0; x < config::mat_width; x++) {

				for (short y = config::mat_height-1; y >0; y--) {

					if (slot[x][y].canBeDestroyed) {

						for (short k = y-1; k >= 0 ; k--) {

							if (!slot[x][k].canBeDestroyed) {

								slot[x][k].pos.y = game_area.y + slot[x][k].pos.height * y;
								slot[x][k].TX.pos.y = slot[x][k].pos.y + slot[x][k].pos.height / 2 - slot[x][k].TX.TX.height / 2;
								Slot auxSl = slot[x][k];
								slot[x][k] = slot[x][y];
								slot[x][y] = auxSl;
								
								break;

							}

						}

					}

				}

			}
			resetSelectedJewelPos();

		}

		void checkHoldClick(const Vector2& mousePos) { 

			for (short x = 0; x < config::mat_width; x++) {

				for (short y = 0; y < config::mat_height; y++) {

					checkClickOnJewel(mousePos, x, y);
				
				}
			
			}

		}

		void setValidToSelect(int x, int y) {

			for (short i = -1; i <= 1; i++) {

				for (short j = -1; j <= 1; j++) {

					if (i + x >= 0 && i + x < config::mat_width) {

						if (j + y >= 0 && j + y < config::mat_height) {

							slot[x+i][y+j].validToSelect = true;

						}

					}

				}

			}

		}

		void cleanValidToSet(int x, int y) {

			for (short i = -1; i <= 1; i++) {

				for (short j = -1; j <= 1; j++) {

					if (i + x >= 0 && i + x < config::mat_width) {

						if (j + y >= 0 && j + y < config::mat_height) {

							slot[x + i][y + j].validToSelect = false;

						}

					}

				}

			}

		}

		void checkClickOnJewel(const Vector2& mousePos, short x, short y) {

			if (CheckCollisionPointRec(mousePos, slot[x][y].pos)) {

				if (x == static_cast<int>(lastSlotClicked.x) && y == static_cast<int>(lastSlotClicked.y)) {return;}

				if (slot[x][y].type == selectedJewel) {

					if (slot[x][y].validToSelect) {

						if (slot[x][y].selected) {
							slot[static_cast<int>(lastSlotClicked.x)][static_cast<int>(lastSlotClicked.y)].selected = false;
							prevSlotClicked = lastSlotClicked;
						}
						else { 
							slot[x][y].selected = true;
							prevSlotClicked = lastSlotClicked;
							PlaySound(jewelTouched);
							std::cout << "Deberia estar haciendo ruidito\n";
						}
						
						cleanValidToSet(lastSlotClicked.x,lastSlotClicked.y);
						lastSlotClicked.x = x;
						lastSlotClicked.y = y;
						std::cout << "ChckOnJewel on " << x << "-" << y << "\n";
					}

					setValidToSelect(mousePos.x, mousePos.y);

				}

			}

		}

		void checkOneTimeClick(const Vector2& mousePos) {

			for (short x = 0; x < config::mat_width; x++) {

				for (short y = 0; y < config::mat_height; y++) {

					if (CheckCollisionPointRec(mousePos, slot[x][y].pos)) {

						selectedJewel = slot[x][y].type;
						lastSlotClicked = { static_cast<float>(x) , static_cast<float>(y) };
						if (!slot[x][y].selected) {
							PlaySound(jewelTouched);
							std::cout << "Deberia estar haciendo ruidito\n";
							slot[x][y].selected = true;
							std::cout << "La gema seleccionada es: ";
							switch (selectedJewel)
							{
							case JEWEL::RUBY:
								std::cout << " un RUBY";
								break;
							case JEWEL::EMERALD:
								std::cout << " una ESMERALDA";
								break;
							case JEWEL::SAPPHIRE:
								std::cout << " un SAFIRO";
								break;
							case JEWEL::TOPAZ:
								std::cout << " un TOPACIO";
								break;
							case JEWEL::NONE:
								std::cout << " INEXISTENTE";
								break;
							}
							std::cout << "\n";
						}
					}

				}

			}

		}
		
		void draw() {

			BeginDrawing();
			ClearBackground(DARKBROWN); //126 113 97 255

			for (short x = 0; x < config::mat_width; x++)
			{
				for (short y = 0; y < config::mat_height; y++)
				{
					
					#if DEBUG
					if (slot[x][y].validToSelect) {
						DrawRectangleLinesEx(slot[x][y].pos, 2, GREEN);
					}
					#endif

					if (!slot[x][y].canBeDestroyed && slot[x][y].active) {
						DrawTX(slot[x][y].TX);
					}

					if (slot[x][y].selected) {
						DrawRectangleLinesEx(slot[x][y].pos, 4, DARKBLUE);
					}

				}

			}

			DrawRectangleLinesEx(game_area, 2, BLACK);

			if (toDoObjetives) {

				using namespace objetive;

				for (short i = 0; i < HowManyJewells; i++) {

					DrawRectangleRec(objetive::objetiveGems[i].hitbox, objetiveBackground);
					DrawTX(objetive::objetiveGems[i]);
					DrawText(&(std::to_string(objetive::gemsRequiered[i]))[0], objetiveText[i].pos.x, objetiveText[i].pos.y, objetiveText[i].size, objetiveText[i].color);

				}

				DrawText(&req.tx[0], req.pos.x, req.pos.y, req.size, req.color);
				DrawText(&(std::to_string(turnCounter))[0], turn.pos.x, turn.pos.y, turn.size, turn.color);
				DrawText(&turnDisplay.tx[0], turnDisplay.pos.x, turnDisplay.pos.y, turnDisplay.size, turnDisplay.color);

			}

			if (pausing) {

				using namespace pause;

				DrawRectangleRec(pRec, pauseBG);
				DrawRectangleLinesEx(pRec, config::squareThickness, BLACK);
				DrawText(&pText.tx[0], pText.pos.x, pText.pos.y, pText.size, pText.color);

				for (short i = 0; i < howManyPauseButtons; i++)	{

					if (pauseButtons[i].selected) {
						pauseButtons[i].color = LIME;
					}
					else {
						pauseButtons[i].color = WHITE;
					}

					drawButton(pauseButtons[i]);

				}

			}
			

			EndDrawing();

		}
		
		void deinit() {

			UnloadTexture(JewelTX[static_cast<int>(JEWEL::RUBY)]);
			UnloadTexture(JewelTX[static_cast<int>(JEWEL::EMERALD)]);
			UnloadTexture(JewelTX[static_cast<int>(JEWEL::SAPPHIRE)]);
			UnloadTexture(JewelTX[static_cast<int>(JEWEL::TOPAZ)]);

			UnloadSound(jewelTouched);
			UnloadSound(comprobatedChain);

		}

		void resetSelectedJewelPos() {

			for (short i = 0; i < config::mat_height*config::mat_width; i++) {

				selectedJewelPos[i] = { -1,-1 };

			}
		}

	}

}