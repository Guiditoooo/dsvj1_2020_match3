#ifndef GEMS_H
#define GEMS_H

#include <iostream>
#include "scenes/game/game_config/game_config.h"

namespace match {

	namespace game {

		const int HowManyJewells = 4;

		enum class JEWEL {
			RUBY,
			EMERALD,
			SAPPHIRE,
			TOPAZ,
			NONE
		};

		extern JEWEL selectedJewel;

		extern Texture2D JewelTX[];

	}

}

#endif