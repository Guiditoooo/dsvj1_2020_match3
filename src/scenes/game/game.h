#ifndef GAME_H
#define GAME_H

#include <string>
#include "raylib.h"
#include "extern/extern.h"
#include "gem_creation/gems.h"
#include "slots_mecanic/slots.h"
#include "game_config/game_config.h"

namespace match {

	namespace game {

		namespace pause {

			const int howManyPauseButtons = 2;

			static Rectangle pRec;
			static Text pText;

			static Button resume;
			static Button mainMenu;

			static Button pauseButtons[howManyPauseButtons];

		}
		
		namespace objetive {

			const Color objetiveBackground = { 170, 125, 35, 255 };
			const int MAX_QTY_TOTAL = 120;
			const int MIN_QTY_TOTAL = 40;
			const int MAX_QTY_EACH = 30;
			const int MIN_TURNS = 5;

			static Texture objetiveGems[HowManyJewells];
			static Text objetiveText[HowManyJewells];
			static Text turnDisplay;
			static Text req;
			static Text turn;
			
			static int gemsRequiered[HowManyJewells];
			static int turnCounter;

		}

		static const Color pauseBG = { 126, 113, 97, 255 }; //Brown
		static const Color pauseButtonsColor = { 156, 143, 127, 255 }; //LightBrown

		static Sound jewelTouched;
		static Sound comprobatedChain;

		extern float frames_expend;

		extern bool pausing;
		extern bool bricked;	

		void init();
		void update();
		void PauseMenuCheck();
		void resetGrid();
		void checkReleaseClick();
		void checkHoldClick(const Vector2& mousePos);
		void checkClickOnJewel(const Vector2& mousePos, short x, short y);
		void draw();
		void deinit();

	}

}
#endif