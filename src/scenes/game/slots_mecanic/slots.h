#ifndef SLOTS_H
#define SLOTS_H

#include "extern/extern.h"
#include "scenes/game/gem_creation/gems.h"

namespace match {

	namespace game {

		extern bool already_created;

		struct Slot {

			Texture TX;
			Rectangle pos;
			JEWEL type = JEWEL::NONE;
			Color color;
			bool selected = false;
			bool active = false;
			bool validToSelect = false;
			bool canBeDestroyed = false;

		};

		extern Slot slot[config::mat_width][config::mat_height];

		void createGem(Slot& slot, int x, int y);
		void rollSlots(float timer, float frames_expend, Slot slot[config::mat_width][config::mat_height], int& sum_x, int& sum_y);
		void clearSlots(Slot slot[config::mat_width][config::mat_height]);

	}

}

#endif