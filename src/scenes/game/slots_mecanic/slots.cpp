#include "slots.h"

namespace match {

	namespace game {

		Slot slot[config::mat_width][config::mat_height];

		bool already_created = false;

		void createGem(Slot& slot, int x, int y) {

			slot.pos.width = game_area.width / config::mat_width;
			slot.pos.height = game_area.height / config::mat_height;
			slot.pos.x = game_area.x + slot.pos.width * x;
			slot.pos.y = game_area.y + slot.pos.height * y;
			slot.type = static_cast<JEWEL>(GetRandomValue(0, 3));

			switch (slot.type)
			{
			case JEWEL::RUBY:
				slot.color = RED;
				slot.TX.TX = JewelTX[static_cast<int>(JEWEL::RUBY)];
				break;
			case JEWEL::EMERALD:
				slot.color = DARKGREEN;
				slot.TX.TX = JewelTX[static_cast<int>(JEWEL::EMERALD)];
				break;
			case JEWEL::SAPPHIRE:
				slot.color = SKYBLUE;
				slot.TX.TX = JewelTX[static_cast<int>(JEWEL::SAPPHIRE)];
				break;
			case JEWEL::TOPAZ:
				slot.color = YELLOW;
				slot.TX.TX = JewelTX[static_cast<int>(JEWEL::TOPAZ)];
				break;
			}

			slot.TX.pos.x = slot.pos.x + slot.pos.width / 2 - slot.TX.TX.width / 2;
			slot.TX.pos.y = slot.pos.y + slot.pos.height / 2 - slot.TX.TX.height / 2;
			slot.active = true;
			slot.canBeDestroyed = false;
			slot.selected = false;
			//std::cout << "Creada gema en: " << x << " - " << y << "\n";

		}

		void rollSlots(float timer, float frames_expend, Slot slot[config::mat_width][config::mat_height], int& sum_x, int& sum_y) {

			if (!already_created) {

				if (frames_expend > timer || IsKeyPressed(KEY_I)) {

					frames_expend = 0;

					int y = sum_y;
					int x = sum_x;

					if (!slot[x][y].active) {

						createGem(slot[x][y], x, y);

						std::cout << "Creada gema en: " << sum_x << " - " << sum_y << "\n";
						sum_x++;

						if (sum_x == config::mat_width) {

							sum_x = 0;
							sum_y++;

						}

					}

				}

			}


			if (slot[config::mat_width - 1][config::mat_height - 1].active && !already_created) {

				already_created = true;
				std::cout << "Ya se crearon to2 \n";

			}

		}

		void clearSlots(Slot slot[config::mat_width][config::mat_height]) {

			for (short x = 0; x < config::mat_width; x++) {

				for (short y = 0; y < config::mat_height; y++) {

					slot[x][y].pos.width = 0;
					slot[x][y].pos.height = 0;
					slot[x][y].pos.x = 0;
					slot[x][y].pos.y = 0;
					slot[x][y].type = JEWEL::NONE;
					slot[x][y].active = false;
					slot[x][y].selected = false;
					slot[x][y].canBeDestroyed = false;

				}

			}

		}

	}

}