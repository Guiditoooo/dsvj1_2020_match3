#ifndef MENU_H
#define MENU_H

#include "raylib.h"
#include"extern/extern.h"

namespace match {

	namespace menu {

		const int baseTextHeight = 30; // 30px
		const int howManyButtons = 4;
		const int titleLenght = 3;

		extern Button buttons[howManyButtons];
		static Text title[titleLenght];

		static Sound buttonBeep;

		extern bool continueInGame;

		extern void init();
		extern void update();
		extern void draw();
		extern void deinit();

	}

}

#endif