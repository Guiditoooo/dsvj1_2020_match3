#ifndef RESOLUTION_H
#define RESOLUTION_H

#include "raylib.h"
#include "extern/extern.h"
#include "scenes/game/game.h"

namespace match {

	namespace resolution {

		const int howManyResolutionButtons = 2;

		static Texture win;
		static Texture lose;

		static Button confirm;
		static Button next;
		static Button resButtons[howManyResolutionButtons];
		static Sound buttonBeep;

		extern void init();
		extern void update();
		extern void draw();
		extern void deinit();

	}

}

#endif