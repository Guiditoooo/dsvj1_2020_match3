#include "resolution.h"

namespace match {

	namespace resolution {

		void init() {

			buttonBeep = LoadSound("res/assets/sound/button beep.ogg");

			win.TX = LoadTexture("res/assets/img/Vict.png");
			win.TX.width = game::game_area.width;
			win.TX.height = game::game_area.height;
			win.pos.x = config::screen::width / 2 - win.TX.width/2;
			
			lose.TX = LoadTexture("res/assets/img/Fail.png");
			lose.TX.width = game::game_area.width;
			lose.TX.height = game::game_area.height;
			lose.pos.x = config::screen::width / 2 - lose.TX.width/2;

			confirm.color = game::objetive::objetiveBackground;
			confirm.txt.tx = "MAIN MENU";
			confirm.txt.size = game::game_area.width / 20;
			confirm.square.width = MeasureText(&confirm.txt.tx[0], confirm.txt.size) * 3 / 2;
			confirm.square.height = confirm.txt.size * 2;
			confirm.square.x = config::screen::width / 2 - confirm.square.width / 2;
			confirm.square.y = config::screen::height * 6 / 10;
			confirm.txt.pos.x = confirm.square.x + confirm.square.width / 2 - MeasureText(&confirm.txt.tx[0],confirm.txt.size) / 2;
			confirm.txt.pos.y = confirm.square.y + confirm.square.height / 2 - confirm.txt.size / 2;
			confirm.txt.color = BLACK;

			next.color = game::objetive::objetiveBackground;
			next.txt.tx = "NEXT LEVEL";
			next.txt.size = game::game_area.width / 20;
			next.square.width = MeasureText(&next.txt.tx[0], next.txt.size) * 3 / 2;
			next.square.height = next.txt.size * 2;
			next.square.x = config::screen::width / 2 - next.square.width / 2;
			next.square.y = config::screen::height * 15 / 20;
			next.txt.pos.x = next.square.x + next.square.width / 2 - MeasureText(&next.txt.tx[0], next.txt.size) / 2;
			next.txt.pos.y = next.square.y + next.square.height / 2 - next.txt.size / 2;
			next.txt.color = BLACK;

			resButtons[0] = confirm;
			resButtons[1] = next;

		}

		void update() {

			Vector2 mousePoint = GetMousePosition();

			for (int i = 0; i < howManyResolutionButtons; i++) {

				if (CheckCollisionPointRec(mousePoint, resButtons[i].square)) {

					resButtons[i].selected = true;

					if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {

						if (i == 0) {
							config::scenes::next_scene = config::scenes::Scene::MENU;
							game::resetGrid();
						}
						else {
							config::scenes::next_scene = config::scenes::Scene::GAME;
							game::resetGrid();
						}

					}

				}
				else {

					resButtons[i].selected = false;

				}

			}
		}

		void draw() {

			BeginDrawing();
			ClearBackground(DARKBLUE);

			if (winLoseCheck == Resolution::LOSE) {
				DrawTX(lose);
			}
			if (winLoseCheck == Resolution::WIN) {
				DrawTX(win);
			}

			for (short i = 0; i < howManyResolutionButtons; i++) {

				resButtons[i].selected ? resButtons[i].txt.color = RED : resButtons[i].txt.color = BLACK;
				drawButton(resButtons[i]);

			}

			EndDrawing();

		}

		void deinit() {

			UnloadSound(buttonBeep);
			UnloadTexture(win.TX);
			UnloadTexture(lose.TX);

		}

	}

}
